import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';

import { AppComponent } from './app.component';
import { SerieComponent } from './serie/serie.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { FormComponent } from './serie/form/form.component';
import { Routes, RouterModule } from '@angular/router';

import { SerieService } from './serie/serie.service';
import { PaginatorComponent } from './paginator/paginator.component';
import { HttpClientModule } from '@angular/common/http';

import { registerLocaleData } from '@angular/common';
import localeES from '@angular/common/locales/es';
import { DetalleComponent } from './serie/detalle/detalle.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatDatepickerModule } from '@angular/material';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { FormsModule } from '@angular/forms';

registerLocaleData(localeES, 'es');

const routes: Routes = [
  { path: '', redirectTo: '/serie', pathMatch: 'full' },
  { path: 'serie', component: SerieComponent },
  { path: 'serie/page/:page', component: SerieComponent },
  { path: 'serie/form', component: FormComponent },
  { path: 'serie/form/:id', component: FormComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    SerieComponent,
    PaginatorComponent,
    DetalleComponent,
    FormComponent 
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot(routes),
    BrowserAnimationsModule, MatDatepickerModule, MatMomentDateModule
  ],
  providers: [SerieService, { provide: LOCALE_ID, useValue: 'es' }],
  bootstrap: [AppComponent]
})
export class AppModule { }
