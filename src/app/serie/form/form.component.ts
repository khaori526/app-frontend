import { Component, OnInit } from '@angular/core';
import { Serie } from '../serie';
import { SerieService } from '../serie.service';
import { Router, ActivatedRoute } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html'
})
export class FormComponent implements OnInit {

  private serie: Serie = new Serie();
  titulo: string = "Crear serie";

  errores: string[];

  constructor(private serieService: SerieService,
    private router: Router,
    private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.activatedRoute.paramMap.subscribe(params => {
      let id = +params.get('id');
      if (id) {
        this.serieService.getSerie(id).subscribe((serie) => this.serie = serie);
      }
    });

    // this.serieService.getRegiones().subscribe(regiones => this.regiones = regiones);
  }

  create(): void {
    console.log(this.serie);
    this.serieService.create(this.serie)
      .subscribe(
        serie => {
          this.router.navigate(['/serie']);
          Swal.fire('Nueva serie', `La serie ${serie.title} ha sido creado con éxito`, 'success');
        },
        err => {
          this.errores = err.error.errors as string[];
          console.error('Código del error desde el backend: ' + err.status);
          console.error(err.error.errors);
        }
      );
  }

  update(): void {
    console.log(this.serie);
    this.serieService.update(this.serie)
      .subscribe(
        response => {
          this.router.navigate(['/clientes']);
          Swal.fire('Serie Actualizada', `${response.mensaje}: ${response.serie.title}`, 'success');
        },
        err => {
          this.errores = err.error.errors as string[];
          console.error('Código del error desde el backend: ' + err.status);
          console.error(err.error.errors);
        }
      )
  }


}
