import { Component, OnInit } from '@angular/core';
import { Serie } from './serie';
import { SerieService } from './serie.service';
import { ActivatedRoute } from '@angular/router';
import { tap } from 'rxjs/operators';
import { ModalService } from './detalle/modal.service';
import Swal from 'sweetalert2';
import { SweetAlertOptions } from 'sweetalert2';

@Component({
  selector: 'app-serie',
  templateUrl: './serie.component.html'
})
export class SerieComponent implements OnInit {

  series: Serie[];
  paginador: any;
  serieSeleccionado: Serie;

  constructor(private serieService: SerieService,
    private activatedRoute: ActivatedRoute,
    private modalService: ModalService) { }

  ngOnInit() {

    this.activatedRoute.paramMap.subscribe(params => {
      let page: number = +params.get('page');

      if (!page) {
        page = 0;
      }

      this.serieService.getSeries(page)
        .pipe(
          tap(response => {
            console.log('SerieComponent: tap 3');
            (response.series.content as Serie[]).forEach(serie => console.log(serie.title));
          })
        ).subscribe(response => {
          this.series = response.series.content as Serie[];
          this.paginador = response.series;
        });

        this.modalService.notificarUpload.subscribe(serie => {
          this.series = this.series.map(serieOriginal => {
            if (serie.id == serieOriginal.id) {
              serieOriginal.foto = serie.foto;
            }
            return serieOriginal;
          })
        })
    });

  }

  delete(serie: Serie): void {

    const options = {
      title: 'Está seguro?',
      text: `¿Seguro que desea eliminar la serie ${serie.title} ?`,
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, eliminar!',
      cancelButtonText: 'No, cancelar!',
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: false,
      reverseButtons: true
  } as SweetAlertOptions;

    Swal.fire(
      options
    ).then((result) => {
      if (result.value) {

        this.serieService.delete(serie.id).subscribe(
          () => {
            this.series = this.series.filter(ser => ser !== serie)
            Swal.fire(
              'Serie Eliminada!',
              `Serie ${serie.title} eliminado con éxito.`,
              'success'
            )
          }
        )

      }
    });
  }

  abrirModal(serie: Serie) {

    this.serieService.getSerie(serie.id).subscribe(response => {
      this.serieSeleccionado = response.serie;
      this.modalService.abrirModal();
    });

    
  }

}
