import { SeasonSerieKey } from './season_serie_key';

export class SeasonSerie{
    id: SeasonSerieKey;
    episodes: number;
}