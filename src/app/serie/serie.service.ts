import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpRequest, HttpEvent } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { map, tap, catchError } from 'rxjs/operators';
import { Serie } from './serie';
import swal from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class SerieService {

  private urlEndPoint: string = 'http://localhost:8090/app/serie';

  private httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' });

  constructor(private http: HttpClient, private router: Router) { }

  getSeries(page: number): Observable<any> {
    return this.http.get(this.urlEndPoint + '/findAll/page/' + page).pipe(
      tap((response: any) => {
        console.log('SerieService: tap 1');
        (response.series.content as Serie[]).forEach(serie => console.log(serie.title));
      }),
      map((response: any) => {
        (response.series.content as Serie[]).map(serie => {
          serie.title = serie.title.toUpperCase();
          //let datePipe = new DatePipe('es');
          //cliente.createAt = datePipe.transform(cliente.createAt, 'EEEE dd, MMMM yyyy');
          //cliente.createAt = formatDate(cliente.createAt, 'dd-MM-yyyy', 'es');
          return serie;
        });
        return response;
      }),
      tap(response => {
        console.log('SerieService: tap 2');
        (response.series.content as Serie[]).forEach(serie => console.log(serie.title));
      })
    );
  }

  getSerie(id): Observable<any> {
    return this.http.get<Serie>(`${this.urlEndPoint}/ver/${id}`).pipe(
      catchError(e => {

        this.router.navigate(['/serie']);
        console.error(e.error.mensaje);
        swal.fire('Error al visualizar la serie', e.error.mensaje, 'error');
        return throwError(e);
      })
    );
  }

  create(serie: Serie): Observable<Serie> {
    return this.http.post(this.urlEndPoint + "/create", serie)
      .pipe(
        map((response: any) => response.serie as Serie),
        catchError(e => {

          if (e.status == 400) {
            return throwError(e);
          }

          console.error(e.error.mensaje);
          swal.fire(e.error.mensaje, e.error.error, 'error');
          return throwError(e);
        })
      );
  }

  update(serie: Serie): Observable<any> {
    return this.http.put<any>(`${this.urlEndPoint}/update/${serie.id}`, serie).pipe(
      catchError(e => {


        if (e.status == 400) {
          return throwError(e);
        }

        console.error(e.error.mensaje);
        swal.fire(e.error.mensaje, e.error.error, 'error');
        return throwError(e);
      })
    );
  }

  delete(id: number): Observable<Serie> {
    return this.http.delete<Serie>(`${this.urlEndPoint}/delete/${id}`).pipe(
      catchError(e => {


        console.error(e.error.mensaje);
        swal.fire(e.error.mensaje, e.error.error, 'error');
        return throwError(e);
      })
    );
  }

  subirFoto(archivo: File, id): Observable<HttpEvent<{}>> {

    let formData = new FormData();
    formData.append("archivo", archivo);
    formData.append("id", id);

    let httpHeaders = new HttpHeaders();

    const req = new HttpRequest('POST', `${this.urlEndPoint}/upload`, formData, {
      reportProgress: true,
      headers: httpHeaders
    });

    return this.http.request(req);

  }

}
