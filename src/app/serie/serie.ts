export class Serie {
    id: number;
    title: string;
    description: string;
    premiereDate: string;
    foto: string;
  }
  